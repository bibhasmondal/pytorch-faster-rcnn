import torch
from torch import nn
from backbone import Backbone
from anchor import AnchorGenerator
from collections import OrderedDict
from poolers import MultiScaleRoIAlign
from rpn import RPNHead, RegionProposalNetwork
from roi_heads import BoxHead, BoxPredictor, RoIHeads

class FasterRCNN(nn.Module):
    def __init__(self, num_classes = None, backbone = None,
                 # RPN parameters
                 rpn_anchor_generator = None, rpn_head = None,
                 rpn_pre_nms_top_n_train = 2000, rpn_pre_nms_top_n_test = 1000,
                 rpn_post_nms_top_n_train = 2000, rpn_post_nms_top_n_test = 1000,
                 rpn_nms_thresh = 0.7,
                 rpn_fg_iou_thresh = 0.7, rpn_bg_iou_thresh = 0.3,
                 rpn_num_samples = 256, rpn_positive_fraction = 0.5,
                 # Box parameters
                 box_roi_pool = None, box_head = None, box_predictor = None,
                 box_score_thresh = 0.05, box_nms_thresh = 0.5, box_detections_per_img = 32,
                 box_fg_iou_thresh = 0.5, box_bg_iou_thresh = 0.5,
                 box_num_samples = 128, box_positive_fraction = 0.25,
                 bbox_reg_weights = None):

        super(FasterRCNN, self).__init__()

        if backbone is None:
            backbone = Backbone(True)

        if not hasattr(backbone, "out_channels"):
            raise ValueError("backbone should contain an attribute out_channels")

        assert isinstance(rpn_anchor_generator, (AnchorGenerator, type(None)))
        assert isinstance(box_roi_pool, (MultiScaleRoIAlign, type(None)))

        if num_classes is not None:
            if box_predictor is not None:
                raise ValueError("num_classes should be None when box_predictor is specified")
        else:
            if box_predictor is None:
                raise ValueError("num_classes should not be None when box_predictor is not specified")

        out_channels = backbone.out_channels
        self.backbone = backbone

        if rpn_anchor_generator is None:
            anchor_sizes = ((32,), (64,), (128,), (256,), (512,))
            aspect_ratios = ((0.5, 1.0, 2.0),) * len(anchor_sizes)
            rpn_anchor_generator = AnchorGenerator(anchor_sizes, aspect_ratios)

        if rpn_head is None:
            rpn_head = RPNHead(out_channels, rpn_anchor_generator.num_anchors_per_location()[0])

        rpn_pre_nms_top_n = dict(training = rpn_pre_nms_top_n_train, testing = rpn_pre_nms_top_n_test)
        rpn_post_nms_top_n = dict(training = rpn_post_nms_top_n_train, testing = rpn_post_nms_top_n_test)

        self.rpn = RegionProposalNetwork(
            rpn_anchor_generator, rpn_head,
            rpn_fg_iou_thresh, rpn_bg_iou_thresh,
            rpn_num_samples, rpn_positive_fraction,
            rpn_pre_nms_top_n, rpn_post_nms_top_n, rpn_nms_thresh)

        if box_roi_pool is None:
            box_roi_pool = MultiScaleRoIAlign(featmap_names = [0, 1, 2, 3], output_size = 7, sampling_ratio = 2)

        if box_head is None:
            resolution = box_roi_pool.output_size[0]
            representation_size = 1024
            box_head = BoxHead(out_channels * resolution ** 2, representation_size)

        if box_predictor is None:
            representation_size = 1024
            box_predictor = BoxPredictor(representation_size, num_classes)

        self.roi_heads = RoIHeads(
            # Box
            box_roi_pool, box_head, box_predictor,
            box_fg_iou_thresh, box_bg_iou_thresh,
            box_num_samples, box_positive_fraction,
            bbox_reg_weights,
            box_score_thresh, box_nms_thresh, box_detections_per_img)

    def forward(self, images, bboxes = None):
        feature_maps = self.backbone(images)
        if isinstance(feature_maps, torch.Tensor):
            feature_maps = OrderedDict([(0, feature_maps)])
        proposals_manager = self.rpn(images, feature_maps)
        proposed_boxes = proposals_manager['boxes']
        if bboxes is not None:
            proposed_boxes = torch.cat((proposed_boxes, bboxes.to(proposed_boxes)), dim = 1)
        detections_manager = self.roi_heads(feature_maps, proposed_boxes, images.shape[-2:])
        return proposals_manager, detections_manager
