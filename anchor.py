import torch
from torch import nn

class AnchorGenerator(nn.Module):
    def __init__(self, sizes=(128, 256, 512), aspect_ratios=(0.5, 1.0, 2.0)):
        super(AnchorGenerator, self).__init__()

        if not isinstance(sizes[0], (list, tuple)):
            sizes = tuple((s,) for s in sizes)

        if not isinstance(aspect_ratios[0], (list, tuple)):
            aspect_ratios = (aspect_ratios,) * len(sizes)

        assert len(sizes) == len(aspect_ratios)

        self.sizes = sizes
        self.aspect_ratios = aspect_ratios
        self._cache = {}

    def generate_anchors(self, scales, aspect_ratios, device):
        scales = torch.as_tensor(scales, dtype=torch.float32, device=device)
        aspect_ratios = torch.as_tensor(aspect_ratios, dtype=torch.float32, device=device)
        h_ratios = torch.sqrt(aspect_ratios)
        w_ratios = 1 / h_ratios

        ws = (w_ratios[:, None] * scales[None, :]).view(-1)
        hs = (h_ratios[:, None] * scales[None, :]).view(-1)

        base_anchors = torch.stack([-ws, -hs, ws, hs], dim=1) / 2
        return base_anchors.round()

    def generate_cell_anchors(self, device):
        cell_anchors = []
        for sizes, aspect_ratios in zip(self.sizes, self.aspect_ratios):
            cell_anchors.append(self.generate_anchors(sizes, aspect_ratios, device))
        return cell_anchors

    def generate_shift_anchors(self, grid_sizes, strides, device):
        shift_anchors = []
        for size, stride in zip(grid_sizes, strides):
            grid_height, grid_width = size
            stride_height, stride_width = stride
            shifts_x = torch.arange(0, grid_width, dtype = torch.float32, device = device) * stride_width
            shifts_y = torch.arange(0, grid_height, dtype = torch.float32, device = device) * stride_height
            shift_y, shift_x = torch.meshgrid(shifts_y, shifts_x)
            shift_x = shift_x.reshape(-1)
            shift_y = shift_y.reshape(-1)
            shifts = torch.stack((shift_x, shift_y, shift_x, shift_y), dim=1)
            shift_anchors.append(shifts)
        return shift_anchors

    def num_anchors_per_location(self):
        return [len(s) * len(a) for s, a in zip(self.sizes, self.aspect_ratios)]

    def grid_anchors(self, grid_sizes, strides, device):
        shift_anchors = self.generate_shift_anchors(grid_sizes, strides, device)
        base_anchors = self.generate_cell_anchors(device)
        anchors = []
        for shift, base in zip(shift_anchors, base_anchors):
            anchors.append((shift.unsqueeze(dim = 1) + base.unsqueeze(dim = 0)).view(-1, 4))
        return torch.cat(anchors, dim = 0)

    def cached_grid_anchors(self, grid_sizes, strides, device = "cpu"):
        key = tuple(grid_sizes) + tuple(strides)
        if key in self._cache:
            return self._cache[key]
        anchors = self.grid_anchors(grid_sizes, strides, device)
        self._cache[key] = anchors
        return anchors

    @staticmethod
    def assign_targets(target_matcher, anchors, bboxes):
        matches = target_matcher(anchors, bboxes)
        # get the targets corresponding GT for each proposal
        # NB: need to clamp the indices because we can have a single
        # GT in the image, and matched_idxs can be -2, which goes
        # out of bounds
        batch_idx = torch.arange(bboxes.size(0))[:, None]
        gt_anchors = bboxes[batch_idx, matches.clamp(min = 0)]
        # Foreground (positive examples)
        gt_labels = matches.clamp(max = 1)
        gt_labels[gt_labels.eq(0)] = 1
        # Background (negative examples)
        gt_labels[matches.eq(target_matcher.BELOW_LOW_THRESHOLD)] = 0
        return gt_anchors, gt_labels

    def forward(self, grid_sizes, image_size, batch_size, device):
        strides = tuple((image_size[0] / g[0], image_size[1] / g[1]) for g in grid_sizes)
        anchors = self.cached_grid_anchors(grid_sizes, strides, device)
        anchors = anchors.repeat(batch_size, 1, 1)
        return anchors
