import torch
from torch import nn
from proposal import ProposalCreator
from utils import Matcher, BalancedPositiveNegativeSampler, BoxCoder, BoxManager

class RPNHead(nn.Module):
    def __init__(self, in_channels, num_anchors):
        super(RPNHead, self).__init__()
        self.conv = nn.Conv2d(in_channels, in_channels, kernel_size=3, stride=1, padding=1)
        self.cls_logits = nn.Conv2d(in_channels, num_anchors, kernel_size=1, stride=1)
        self.bbox_pred = nn.Conv2d(in_channels, num_anchors * 4, kernel_size=1, stride=1)

        for l in self.children():
            torch.nn.init.normal_(l.weight, std=0.01)
            torch.nn.init.constant_(l.bias, 0)

    def forward(self, feature_maps):
        bbox_reg = []
        logits = []
        for feature_map in feature_maps:
            feature = torch.relu(self.conv(feature_map))
            deltas = self.bbox_pred(feature)
            deltas = deltas.permute(0, 2, 3, 1)
            deltas = deltas.reshape(deltas.shape[0], -1, 4)
            bbox_reg.append(deltas)
            scores = self.cls_logits(feature)
            scores = scores.permute(0, 2, 3, 1)
            scores = scores.reshape(scores.shape[0], -1)
            logits.append(scores)
        bbox_reg = torch.cat(bbox_reg, dim = 1)
        logits = torch.cat(logits, dim = 1)
        return bbox_reg, logits

class RegionProposalNetwork(nn.Module):
    def __init__(self, anchor_generator, head,
                 #
                 fg_iou_thresh, bg_iou_thresh,
                 num_samples, positive_fraction,
                 #
                 pre_nms_top_n, post_nms_top_n, nms_thresh):

        super(RegionProposalNetwork, self).__init__()
        self.anchor_generator = anchor_generator
        self.head = head
        # used during training
        self.target_matcher = Matcher(fg_iou_thresh, bg_iou_thresh, allow_low_quality_matches = True)
        self.fg_bg_sampler = BalancedPositiveNegativeSampler(num_samples, positive_fraction)
        self.box_coder = BoxCoder(weights=(1.0, 1.0, 1.0, 1.0))
        self.proposal_creator = ProposalCreator(self.box_coder, pre_nms_top_n, post_nms_top_n, nms_thresh)
        # used during testing
        self._pre_nms_top_n = pre_nms_top_n
        self._post_nms_top_n = post_nms_top_n
        self.nms_thresh = nms_thresh

    def forward(self, images, feature_maps):
        # RPN uses all feature maps that are available
        feature_maps = list(feature_maps.values())
        pred_bbox_deltas, objectness = self.head(feature_maps)
        # Create prpopsals
        grid_sizes = tuple([feature_map.shape[-2:] for feature_map in feature_maps])
        anchors = self.anchor_generator(grid_sizes, images.shape[-2:], objectness.size(0), objectness.device)

        num_anchors_per_lvl = []
        for (h, w), num_anchors_per_loc in zip(grid_sizes, self.anchor_generator.num_anchors_per_location()):
            num_anchors_per_lvl.append(h * w * num_anchors_per_loc)
        proposals, fg_scores = self.proposal_creator(pred_bbox_deltas, objectness, anchors, num_anchors_per_lvl, images.shape[-2:])

        kwargs = {
            'src_processed_data': dict(boxes = proposals, scores = fg_scores),
            'dst_boxes': anchors,
            'src_deltas': pred_bbox_deltas,
            'src_scores': objectness
        }
        proposals_manager = BoxManager(self.target_matcher, self.fg_bg_sampler, self.box_coder, **kwargs)
        return proposals_manager
