from torch import nn
from torchvision.models.detection.backbone_utils import resnet_fpn_backbone

class Backbone(nn.Module):
    def __init__(self, pretrained = False):
        super(Backbone, self).__init__()
        self.model = resnet_fpn_backbone('resnet50', pretrained)
        self.out_channels = self.model.out_channels

    def forward(self, images):
        feature_maps = self.model(images)
        return feature_maps
