import torch
from torch import nn
import torch.nn.functional as F

class DetectionMaker(nn.Module):
    def __init__(self, box_coder, score_thresh, nms_thresh, detections_per_img):
        super(DetectionMaker, self).__init__()
        self.box_coder = box_coder
        self.score_thresh = score_thresh
        self.nms_thresh = nms_thresh
        self.detections_per_img = detections_per_img

    def make_detections(self, pred_bbox_deltas, proposals):
        detections = self.box_coder.decode(pred_bbox_deltas.detach(), proposals)
        return detections

    def postprocess_detections(self, pred_boxes, class_logits, proposals, image_size):
        num_classes = class_logits.shape[-1]
        pred_boxes = pred_boxes.reshape(*pred_boxes.shape[:-1], -1, 4)
        pred_scores = torch.softmax(class_logits, -1)

        final_boxes = []
        final_scores = []
        final_labels = []
        for boxes, scores in zip(pred_boxes, pred_scores):
            boxes = self.box_coder.clip_boxes_to_image(boxes, image_size)
            # create labels for each prediction
            labels = torch.arange(num_classes, device = scores.device)
            labels = labels.view(1, -1).expand_as(scores)
            # remove predictions with the background label
            boxes = boxes[:, 1:]
            scores = scores[:, 1:]
            labels = labels[:, 1:]
            # batch everything, by making every class prediction be a separate instance
            boxes = boxes.reshape(-1, 4)
            scores = scores.flatten()
            labels = labels.flatten()
            # remove low scoring boxes
            inds = torch.nonzero(scores > self.score_thresh).squeeze(1)
            boxes, scores, labels = boxes[inds], scores[inds], labels[inds]
            # remove empty boxes
            keep = self.box_coder.remove_small_boxes(boxes, min_size = 1e-2)
            boxes, scores, labels = boxes[keep], scores[keep], labels[keep]
            # non-maximum suppression, independently done per class
            keep = self.box_coder.batched_nms(boxes, scores, labels, self.nms_thresh)
            # keep only topk scoring predictions
            keep = keep[:self.detections_per_img]
            boxes, scores, labels = boxes[keep], scores[keep], labels[keep]
            # add padding to match shape and append
            final_boxes.append(F.pad(boxes, [0, self.detections_per_img - keep.numel(), 0, 0], 'constant', 0.0))
            final_scores.append(F.pad(scores, [0, self.detections_per_img - keep.numel()], 'constant', 0.0))
            final_labels.append(F.pad(labels, [0, self.detections_per_img - keep.numel()], 'constant', 0))

        final_boxes = torch.stack(final_boxes, dim = 0)
        final_scores = torch.stack(final_scores, dim = 0)
        final_labels = torch.stack(final_labels, dim = 0)
        return final_boxes, final_scores, final_labels

    def forward(self, pred_bbox_deltas, class_logits, proposals, image_size):
        detections = self.make_detections(pred_bbox_deltas, proposals)
        detections, scores, labels = self.postprocess_detections(detections, class_logits, proposals, image_size)
        return detections, scores, labels
