import torch
from torch import nn
from detection import DetectionMaker
from utils import Matcher, BalancedPositiveNegativeSampler, BoxCoder, BoxManager

class BoxHead(nn.Module):
    def __init__(self, in_channels, representation_size):
        super(BoxHead, self).__init__()
        self.fc6 = nn.Linear(in_channels, representation_size)
        self.fc7 = nn.Linear(representation_size, representation_size)

    def forward(self, x):
        x = x.flatten(start_dim = -3)
        x = torch.relu(self.fc6(x))
        x = torch.relu(self.fc7(x))
        return x

class BoxPredictor(nn.Module):
    def __init__(self, in_channels, num_classes):
        super(BoxPredictor, self).__init__()
        self.cls_score = nn.Linear(in_channels, num_classes)
        self.bbox_pred = nn.Linear(in_channels, num_classes * 4)

    def forward(self, x):
        scores = self.cls_score(x)
        bbox_deltas = self.bbox_pred(x)
        return bbox_deltas, scores

class RoIHeads(nn.Module):
    def __init__(self, box_roi_pool,
                 box_head, box_predictor,
                 # Faster R-CNN training
                 fg_iou_thresh, bg_iou_thresh,
                 num_samples, positive_fraction,
                 bbox_reg_weights,
                 # Faster R-CNN inference
                 score_thresh, nms_thresh, detections_per_img):

        super(RoIHeads, self).__init__()
        self.box_roi_pool = box_roi_pool
        self.box_head = box_head
        self.box_predictor = box_predictor
        # used during training
        self.target_matcher = Matcher(fg_iou_thresh, bg_iou_thresh, allow_low_quality_matches = False)
        self.fg_bg_sampler = BalancedPositiveNegativeSampler(num_samples, positive_fraction)
        if bbox_reg_weights is None:
            bbox_reg_weights = (10., 10., 5., 5.)
        self.box_coder = BoxCoder(weights = bbox_reg_weights)
        self.detection_maker = DetectionMaker(self.box_coder, score_thresh, nms_thresh, detections_per_img)

    def forward(self, feature_maps, proposals, image_size):
        box_features = self.box_roi_pool(feature_maps, proposals, image_size)
        box_features = self.box_head(box_features)
        box_regression, class_logits = self.box_predictor(box_features)
        detections, class_scores, class_labels = self.detection_maker(box_regression, class_logits, proposals, image_size)
        kwargs = {
            'src_processed_data': dict(boxes = detections, scores = class_scores, labels = class_labels),
            'dst_boxes': proposals,
            'src_deltas': box_regression,
            'src_scores': class_logits
        }
        detections_manager = BoxManager(self.target_matcher, self.fg_bg_sampler, self.box_coder, **kwargs)
        return detections_manager
